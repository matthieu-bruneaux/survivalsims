### * Setup

TOP_DIR=$(shell git rev-parse --show-toplevel)

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** install

## install : install the R package locally
.PHONY: install
install:
	@printf "\n"
	@printf "$(GREEN)*** Installing the package ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'devtools::install()'

### ** pkgdown

## pkgdown : build the pkgdown website locally
.PHONY: pkgdown
pkgdown:
	@printf "\n"
	@printf "$(GREEN)*** Building package website with pkgdown ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'pkgdown::build_site()'

### ** clean

## clean : remove all files that can be generated automatically
.PHONY: clean
clean:
	@printf "\n"
	@printf "$(GREEN)*** Cleaning-up files ***$(NC)\n"
	@printf "\n"
	rm -fr docs/
