##
## Overview

This repository is part of a project with Daniel Padfield aiming at writing a primer about survival analysis for microbiologists.

The main goal of the whole project is to provide non-statistician researchers with an easy-to-follow guide about best practices for survival analysis, and most importantly to help to avoid mistakes that would invalidate the conclusions drawn from misused statistical models.

This repository was created to deal with simulation studies in particular. Many datasets are generated and analyzed in different ways to illustrate how to appropriately use different models.

## Resources

The [Cran Task View](https://cran.r-project.org/web/views/Survival.html) about survival analysis links to a huge amount of R packages aimed at analyzing, visualizing, and simulating data.

Some R packages of interest:

- [coxed](https://cran.r-project.org/package=coxed): Functions for generating, simulating, and visualizing expected durations and marginal changes in duration from the Cox proportional hazards model as described in Kropko and Harden (2017) and Harden and Kropko (2018).
- [simsurv](https://cran.r-project.org/package=simsurv): Simulate survival times from standard parametric survival distributions (exponential, Weibull, Gompertz), 2-component mixture distributions, or a user-defined hazard, log hazard, cumulative hazard, or log cumulative hazard function. Baseline covariates can be included under a proportional hazards assumption.
- [flexsurv](https://cran.r-project.org/package=flexsurv): Flexible parametric models for time-to-event data, including the Royston-Parmar spline model, generalized gamma and generalized F distributions. Any user-defined parametric distribution can be fitted, given at least an R function defining the probability density or hazard.

Other material:

- [Survival analysis with Poisson regression](https://lisamr.github.io/Survival_tutorial/) (tutorial).

## Contact

- [Daniel Padfield](mailto:D.Padfield@exeter.ac.uk)
- [Matthieu Bruneaux](mailto:matthieu.bruneaux@gmail.com)
